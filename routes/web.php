<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $posts = \App\Models\Post::all()->sortByDesc('id');
    return view('welcome', ['posts' => $posts]);
});


Auth::routes();

Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile');
Route::get('/profile/{username}', [App\Http\Controllers\ProfileController::class, 'search']);
Route::get('/profile/{username}/edit', [App\Http\Controllers\ProfileController::class, 'view_edit']);
Route::get('/profile/{username}/gen-api', [App\Http\Controllers\ProfileController::class, 'gen_api']);
Route::post('/editprofile', [App\Http\Controllers\ProfileController::class, 'edit'])->name('edit_profile');

Route::get('/adminpanel', [App\Http\Controllers\AdminPanelController::class, 'index'])->name('admin_panel');
Route::post('/adminpanel', [App\Http\Controllers\AdminPanelController::class, 'req']);
Route::get('/adminpanel/posts', [App\Http\Controllers\AdminPanelController::class, 'posts'])->name('admin_panel.posts');
Route::post('/adminpanel/posts', [App\Http\Controllers\AdminPanelController::class, 'postsreq']);
Route::get('/adminpanel/users', [App\Http\Controllers\AdminPanelController::class, 'users'])->name('admin_panel.users');
Route::post('/adminpanel/users', [App\Http\Controllers\AdminPanelController::class, 'usersreq']);

Route::get('/newpost', [App\Http\Controllers\PostValidationController::class, 'createPostForm'])->name('new_post');
Route::post('/newpost', [App\Http\Controllers\PostValidationController::class, 'PostForm']);

Route::get('post/{id}/islikedbyme', [App\Http\Controllers\PostController::class, 'isLikedByMe']);
Route::post('post/like', [App\Http\Controllers\PostController::class, 'like'])->name('like');
Route::get('post/view/{id}', [App\Http\Controllers\PostController::class, 'view']);
Route::get('post/edit/{id}', [App\Http\Controllers\PostController::class, 'view_edit']);
Route::post('post/edit', [App\Http\Controllers\PostController::class, 'edit'])->name('edit_post');
Route::get('post/remove/{id}', [App\Http\Controllers\PostController::class, 'remove']);

Route::get('/_debugbar/assets/stylesheets', [
    'as' => 'debugbar.assets.css',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@css'
]);
Route::get('/_debugbar/assets/javascript', [
    'as' => 'debugbar.assets.js',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@js'
]);
Route::get('/_debugbar/open', [
    'as' => 'debugbar.openhandler',
    'uses' => '\Barryvdh\Debugbar\Controllers\OpenController@handler'
]);