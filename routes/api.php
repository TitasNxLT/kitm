<?php

use App\Models\Like;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/', function(Request $request) {
    return ['status' => 'OK', 'code' => '200'];
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return ['status' => 'OK', 'code' => '200', 'user' => $request->user()];
});
Route::middleware('auth:api')->get('/user/{id}', function (Request $request, $id) {
    if($request->user()->role == 2) {
        return ['status' => 'OK', 'code' => '200', 'user' => User::where('id', $id)->first()];
    } else {
        return ['status' => 'OK', 'code' => '200', 'user' => User::where('id', $id)->first(['id', 'name', 'username'])];
    }
});
Route::middleware('auth:api')->get('/posts', function(Request $request) {
    return ['status' => 'OK', 'code' => '200', 'posts' => Post::all(['id', 'title', 'message', 'author_id', 'created_at'])];
});
Route::middleware('auth:api')->get('/post/{id}', function(Request $request, $id) {
    return ['status' => 'OK', 'code' => '200', 'post' => Post::where('id', $id)->first(['id', 'title', 'message', 'author_id', 'created_at'])];
});
Route::middleware('auth:api')->get('/post/{id}/likes', function(Request $request, $id) {
    $postLikes = Like::where('post_id', $id)->get();
    $likers = [];
    foreach ($postLikes as $postLike) {
        $liker_user = User::where('id', $postLike->user_id)->first();
        $likers[] = ['user' => [
            'id' => $postLike->user_id,
            'username' => $liker_user->username,
            'name' => $liker_user->name],
        'time' => $postLike->created_at];
    }
    return ['status' => 'OK', 'code' => '200', 'likes' => $likers];
});