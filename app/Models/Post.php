<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function likes()
    {
        return $this->belongsToMany('App\Models\User', 'likes');
    }
    public $fillable = ['title', 'message', 'author_id', 'likes'];
}
