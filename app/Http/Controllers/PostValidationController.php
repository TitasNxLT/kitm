<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use File;

class PostValidationController extends Controller
{
    public function createPostForm(Request $request) {
        if(Auth::user() == null) return redirect('/');
        if(Auth::user()->role > 0){
            return view('newpost');
        } else {
            return redirect('/');
        }
    }

    public function PostForm(Request $request) {
        $this->validate($request, [
            'title' => 'required|max:100',
            'message' => 'required|max:255',
            'video' => ['required', 'file', 'max:10240', // Max 10MB
                function($attribute, $value, $fail) {
                    if($value->getClientOriginalExtension() !== 'mp4') {
                        
                        $fail(__('post.only-allowed-types'));
                    }
                },
            ],
        ]);
        $id = Post::create([
            'title' => $request->title,
            'message' => $request->message,
            'author_id' => Auth::id(),
            'likes' => 0,
        ])->id;
        $file = $request->file('video')->storePubliclyAs('public/videos', $id.'.mp4');

        return back()->with('success', __('post.upload-success'));
    }
}
