<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use Illuminate\Validation\Rule;
use File;

class PostController extends Controller
{
    // public function like($post_id){
    //     if(Auth::user() == null) return redirect('/');
    //     // Like::create([
    //     //     'user_id' => Auth::user()->id,
    //     //     'post_id' => $post_id
    //     // ]);
    // }
    public function view($id)
    {
        \Debugbar::info('Loading post: ' . $id);
        $post = Post::findOrFail($id);
        \Debugbar::info('Loaded post: ' . $post->id);
        return view('post', ['post' => $post]);
    }
    public function isLikedByMe($id)
    {
        $post = Post::findOrFail($id);
        if (Like::whereUserId(Auth::id())->wherePostId($post->id)->exists()){
            return 'true';
        }
        return 'false';
    }

    public function like(Request $request)
    {
        $existing_like = Like::wherePostId($request->post)->whereUserId(Auth::id())->first();

        if (is_null($existing_like)) {
            Like::create([
                'post_id' => $request->post,
                'user_id' => Auth::id()
            ]);
        } else {
            if (is_null($existing_like->deleted_at)) {
                $existing_like->delete();
            } else {
                $existing_like->restore();
            }
        }
        return back();
    }

    public function view_edit($id)
    {
        if(Auth::user() == null){
            return redirect('/');
        }
        $post = Post::findOrFail($id);
        if($post->author_id != Auth::user()->id){
            return redirect('/');
        }
        return view('edit_post', ['post' => $post]);
    }

    public function edit(Request $request)
    {
        if(Auth::user() == null){
            return redirect('/');
        }
        $post = Post::findOrFail($request->id);
        if($post->author_id != Auth::user()->id){
            return redirect('/');
        }
        $this->validate($request, [
            'title' => 'required|max:100',
            'message' => 'required|max:255',
            'video' => [Rule::requiredIf($request->filled('video')), 'file', 'max:20480', // Max 10MB = 10240 | 20MB = 20480
                function($attribute, $value, $fail) {
                    if($value->getClientOriginalExtension() !== 'mp4') {
                        $fail('Only allowed media type is .mp4');
                    }
                },
            ],
        ]);
        $post->title = $request->title;
        $post->message = $request->message;
        $post->save();
        if($request->file('video') != null){
            \Debugbar::info('Saving new video: ' . 'public/videos'.$post->id.'.mp4');
            File::delete(public_path('/storage/videos/'.$post->id.'.mp4'));
            $request->file('video')->storePubliclyAs('public/videos', $post->id.'.mp4');
            return back()->with('success', __('post.edit-success'));
        } else {
            \Debugbar::info('Could not find video');
            return back()->with('success', __('post.edit-success'));
        }

    }

    public function remove($id)
    {
        if(Auth::user() == null){
            return redirect('/');
        }
        $post = Post::findOrFail($id);
        if($post->author_id != Auth::user()->id){
            return redirect('/');
        }
        $post->delete();
        return back();
    }
}
