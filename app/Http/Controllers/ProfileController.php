<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    public function index()
    {
        if(Auth::user() == null) {
            return redirect('/');
        } else {
            $creation_time = new Carbon(Auth::user()->created_at);
            $registered_ago = $creation_time->diffForHumans();
            $likes_received = 0;
            $posts = \App\Models\Post::all();
            foreach (Like::all() as $like) {
                if(Post::where('id', $like->post_id)->where('author_id', Auth::user()->id)->count() == 1){
                    $likes_received += 1;
                }
            }
            return view('profile', ['user' => Auth::user(), 'registered_ago' => $registered_ago, 'likes_received' => $likes_received, 'posts' => $posts]);
        }
    }
    public function search($username)
    {
        $user = User::where('username', $username)->firstOrFail();
        $creation_time = new Carbon($user->created_at);
        $registered_ago = $creation_time->diffForHumans();
        $likes_received = 0;
        $posts = \App\Models\Post::all();
        foreach (Like::all() as $like) {
            if(Post::where('id', $like->post_id)->where('author_id', $user->id)->count() == 1){
                $likes_received += 1;
            }
        }
        return view('profile', ['user' => $user, 'registered_ago' => $registered_ago, 'likes_received' => $likes_received, 'posts' => $posts]);
    }
    public function view_edit($username)
    {
        if(Auth::user() == null){
            return redirect('/');
        }
        $user = User::where('username', $username)->firstOrFail();
        if($user->id != Auth::user()->id){
            return redirect('/');
        }
        return view('edit_profile', ['user' => $user]);
    }
    public function edit(Request $request)
    {
        $user = User::findOrFail($request->id);
        if($user->id != Auth::user()->id){
            return redirect('/');
        }
        $this->validate($request, [
            'name' => 'required|max:100',
        ]);

        $user->name = $request->name;
        $user->save();

        return back()->with('success', __('profile.edit-success'));
    }
    public function gen_api(Request $request, $username)
    {
        if(Auth::user()->role == 2) {
            $user = User::where('username', $username)->first();
            if($user->api_token != null){
                return 'User already has api key';
            }
            $user->api_token = Str::random(60);
            $user->save();
            return back();
        }
    }
}
