<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class AdminPanelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user() == null) return redirect('/');
        if(Auth::user()->role > 1){
            $file_size = 0;
            foreach(File::allFiles(storage_path('app/public/videos')) as $file)
            {
                $file_size += $file->getSize();
            }
            $file_size = number_format($file_size / 1048576,2);
            return view('adminpanel', ['file_size' => $file_size]);
        } else {
            return redirect('/');
        }
    }
    public function posts()
    {
        if(Auth::user() == null) return redirect('/');
        if(Auth::user()->role > 1){
            $posts = \App\Models\Post::all();
            return view('adminpanel_posts', ['posts' => $posts]);
        } else {
            return redirect('/');
        }
    }
    public function postsreq(Request $request)
    {
        if(Auth::user() == null) return redirect('/');
        if(Auth::user()->role > 1){
            $post = \App\Models\Post::where('id', $request->post_id)->firstOrFail();
            if($request->action == "remove"){
                $post->delete();
                return back();
            }
        } else {
            return redirect('/');
        }
    }
    public function users()
    {
        if(Auth::user() == null) return redirect('/');
        if(Auth::user()->role > 1){
            $users = \App\Models\User::all();
            return view('adminpanel_users', ['users' => $users]);
        } else {
            return redirect('/');
        }
    }
    public function usersreq(Request $request)
    {
        $executor = \App\Models\User::where('id', $request->executor)->first();
        if($executor->role != 2){
            return redirect('/');
        } else {
            $user = \App\Models\User::where('id', $request->user_id)->first();
            if($request->action == "up"){
                switch ($user->role) {
                    case 0:
                        $user->role = 1;
                        $user->save();
                        return back();
                        break;
                    case 1:
                        $user->role = 2;
                        $user->save();
                        return back();
                        break;
                    default:
                        return back();
                        break;
                }
            } else {
                switch ($user->role) {
                    case 1:
                        $user->role = 0;
                        $user->save();
                        return back();
                        break;
                    case 2:
                        $user->role = 1;
                        $user->save();
                        return back();
                        break;
                    default:
                        return back();
                        break;
                }
            }
        }
    }
}
