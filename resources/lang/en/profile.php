<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Profile Language Lines
    |--------------------------------------------------------------------------
    |
    | No description as of now
    |
    */

    'profile' => 'profile',
    'displayname' => 'Display Name',
    'username' => 'Username',
    'role' => 'Role',
    'user' => 'User',
    'uploader' => 'Uploader',
    'administrator' => 'Administrator',
    'registered-ago' => 'User registered',
    'posts' => 'posts',
    'likes-received' => 'Likes received',
    'profile-name' => ':name\'s',
    'edit-profile' => 'Edit profile',
    'email' => 'E-Mail Address',
    'edit-success' => 'Profile was succesfully edited',
    'api-token' => 'Api token'

];