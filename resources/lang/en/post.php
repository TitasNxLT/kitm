<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Post Language Lines
    |--------------------------------------------------------------------------
    |
    | No description as of now
    |
    */

    'title' => 'Title',
    'message' => 'Message',
    'video-file' => 'Video file',
    'create-post' => 'Create post',
    'edit-post' => 'Edit post',
    'author' => 'Author',
    'created' => 'Created',
    'updated' => 'Updated',
    'liked-by' => 'Liked by',
    'liked-by-you' => 'Liked by you',
    'liked-by-you-and-x-others' => 'Liked by you and :x others',
    'liked-by-x-and-y-others' => 'Liked by :x and :y others',
    'edit-success' => 'Post was successfully edited! (Video might be updated only after 5 seconds)',
    'only-allowed-types' => 'Only allowed media type is .mp4',
    'upload-success' => 'Post was successfully posted!'

];