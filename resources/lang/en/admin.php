<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Language Lines
    |--------------------------------------------------------------------------
    |
    | No description as of now
    |
    */

    'statistics' => 'Statistics',
    'posts' => 'Posts',
    'users' => 'Users',
    'used-space' => 'Used space',
    'displayname' => 'Display Name',
    'username' => 'Username',
    'role' => 'Role',
    'user' => 'User',
    'uploader' => 'Uploader',
    'administrator' => 'Administrator',
    'error' => 'Error',
    'title' => 'Title',
    'author' => 'Author',
    'actions' => 'Actions',
    'api-token' => 'Api token',
    'api-token-not-set' => 'Api token not set',
    'generate-api' => 'Generate api token',
    'revoke-api' => 'Revoke api token'

];