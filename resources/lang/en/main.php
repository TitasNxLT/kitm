<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Language Lines
    |--------------------------------------------------------------------------
    |
    | No description as of now
    |
    */

    'main_page' => 'Main page',
    'new_post' => 'New post',
    'admin_panel' => 'Admin panel',
    'logout' => 'Log Out',

];
