<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => 'Login',
    'email' => 'E-Mail Address',
    'password' => 'Password',
    'rememberme' => 'Remember Me',
    'login-btn' => 'Login',
    'forgot-password' => 'Forgot Your Password?',
    'register' => 'Register',
    'name' => 'Name',
    'username' => 'Username',
    'confirm-password' => 'Confirm Password',
    'register-btn' => 'Register',
    'register-disabled' => 'Registration is disabled.'
];
