<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Post Language Lines
    |--------------------------------------------------------------------------
    |
    | No description as of now
    |
    */

    'title' => 'Pavadinimas',
    'message' => 'Žinutė',
    'video-file' => 'Video failas',
    'create-post' => 'Sukurti postą',
    'edit-post' => 'Redaguoti postą',
    'author' => 'Autorius',
    'created' => 'Sukurta',
    'updated' => 'Atnaujinta',
    'liked-by' => 'Patinka',
    'liked-by-you' => 'Tau patinka',
    'liked-by-you-and-x-others' => 'Patinka tau ir :x kitiems',
    'liked-by-x-and-y-others' => 'Patinka :x ir :y kitiems',
    'edit-success' => 'Postas buvo sėkmingai suredaguotas! (Atnaujinimas gali trukti apie 5 sekundes)',
    'only-allowed-types' => 'Galima kelti tik .mp4 formato failus',
    'upload-success' => 'Postas buvo sėkmingai sukurtas!'

];