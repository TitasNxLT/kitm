<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Language Lines
    |--------------------------------------------------------------------------
    |
    | No description as of now
    |
    */

    'main_page' => 'Pagrindinis puslapis',
    'new_post' => 'Naujas postas',
    'admin_panel' => 'Admin panelė',
    'logout' => 'Atsijungti',

];