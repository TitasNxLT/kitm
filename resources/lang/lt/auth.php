<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Tokių duomenų duomenų bazėje neradome..',
    'password' => 'Netinkamas slaptažodis.',
    'throttle' => 'Per daug prisijungimų. Pabandykite dar kartą už :seconds sekundžių.',
    'login' => 'Prisijungimas',
    'email' => 'El. paštas',
    'password' => 'Slaptažodis',
    'rememberme' => 'Prisiminti prisijungimą',
    'login-btn' => 'Prisijungti',
    'forgot-password' => 'Pamiršote slaptažodį?',
    'register' => 'Registracija',
    'name' => 'Vardas',
    'username' => 'Slapyvardis',
    'confirm-password' => 'Patvirtinti slaptažodį',
    'register-btn' => 'Registruotis',
    'register-disabled' => 'Registracija yra išjungta.'

];
