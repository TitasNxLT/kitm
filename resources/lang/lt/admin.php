<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Language Lines
    |--------------------------------------------------------------------------
    |
    | No description as of now
    |
    */

    'statistics' => 'Statistika',
    'posts' => 'Postai',
    'users' => 'Vartotojai',
    'used-space' => 'Užimta vieta',
    'displayname' => 'Vardas',
    'username' => 'Slapyvardis',
    'role' => 'Rolė',
    'user' => 'Vartotojas',
    'uploader' => 'Įkėlėjas',
    'administrator' => 'Administratorius',
    'error' => 'Klaida',
    'title' => 'Pavadinimas',
    'author' => 'Autorius',
    'actions' => 'Veiksmai',
    'api-token' => 'Api raktas',
    'api-token-not-set' => 'Api raktas nesugeneruotas',
    'generate-api' => 'Generuoti api raktą',
    'revoke-api' => 'Atimti api raktą'

];