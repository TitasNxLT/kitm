<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Profile Language Lines
    |--------------------------------------------------------------------------
    |
    | No description as of now
    |
    */

    'profile' => 'profilis',
    'displayname' => 'Vardas',
    'username' => 'Slapyvardis',
    'role' => 'Rolė',
    'user' => 'Vartotojas',
    'uploader' => 'Įkėlėjas',
    'administrator' => 'Administratorius',
    'registered-ago' => 'Varotojas užsiregistravo',
    'posts' => 'postai',
    'likes-received' => 'Gauti likes',
    'profile-name' => ':name',
    'edit-profile' => 'Redaguoti profilį',
    'email' => 'El. paštas',
    'edit-success' => 'Profilis buvo sėkmingai redaguotas',
    'api-token' => 'Api raktas'

];