<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute turi būti pažymėtas.',
    'active_url' => 'The :attribute is not a valid URL.', // not used
    'after' => 'The :attribute must be a date after :date.', // not used
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.', // not used
    'alpha' => 'The :attribute may only contain letters.', // not used
    'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.', // not used
    'alpha_num' => 'The :attribute may only contain letters and numbers.', // not used
    'array' => 'The :attribute must be an array.', // not used
    'before' => 'The :attribute must be a date before :date.', // not used
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.', // not used
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.', // not used
        'file' => 'The :attribute must be between :min and :max kilobytes.', // not used
        'string' => 'The :attribute must be between :min and :max characters.', // not used
        'array' => 'The :attribute must have between :min and :max items.', // not used
    ],
    'boolean' => 'The :attribute field must be true or false.', // not used
    'confirmed' => ':attribute patvirtinimas nesutampa.',
    'date' => 'The :attribute is not a valid date.', // not used
    'date_equals' => 'The :attribute must be a date equal to :date.', // not used
    'date_format' => 'The :attribute does not match the format :format.', // not used
    'different' => 'The :attribute and :other must be different.', // not used
    'digits' => 'The :attribute must be :digits digits.', // not used
    'digits_between' => 'The :attribute must be between :min and :max digits.', // not used
    'dimensions' => 'The :attribute has invalid image dimensions.', // not used
    'distinct' => 'The :attribute field has a duplicate value.', // not used
    'email' => 'The :attribute must be a valid email address.',
    'ends_with' => 'The :attribute must end with one of the following: :values.', // not used
    'exists' => 'The selected :attribute is invalid.', // not used
    'file' => 'The :attribute must be a file.', // not used
    'filled' => ':attribute laukas turi būti užpildytas.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.', // not used
        'file' => 'The :attribute must be greater than :value kilobytes.', // not used
        'string' => ':attribute turi būti ilgesnis nei :value raidės.',
        'array' => 'The :attribute must have more than :value items.', // not used
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.', // not used
        'file' => 'The :attribute must be greater than or equal :value kilobytes.', // not used
        'string' => 'The :attribute must be greater than or equal :value characters.', // not used
        'array' => 'The :attribute must have :value items or more.', // not used
    ],
    'image' => 'The :attribute must be an image.', // not used
    'in' => 'The selected :attribute is invalid.', // not used
    'in_array' => 'The :attribute field does not exist in :other.', // not used
    'integer' => 'The :attribute must be an integer.', // not used
    'ip' => 'The :attribute must be a valid IP address.', // not used
    'ipv4' => 'The :attribute must be a valid IPv4 address.', // not used
    'ipv6' => 'The :attribute must be a valid IPv6 address.', // not used
    'json' => 'The :attribute must be a valid JSON string.', // not used
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.', // not used
        'file' => ':attribute turi būti mažesnis nei :value kB.',
        'string' => 'The :attribute must be less than :value characters.', // not used
        'array' => 'The :attribute must have less than :value items.', // not used
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.', // not used
        'file' => 'The :attribute must be less than or equal :value kilobytes.', // not used
        'string' => 'The :attribute must be less than or equal :value characters.', // not used
        'array' => 'The :attribute must not have more than :value items.', // not used
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.', // not used
        'file' => ':attribute negali būti didesnis nei :max kB.',
        'string' => 'The :attribute may not be greater than :max characters.', // not used
        'array' => 'The :attribute may not have more than :max items.', // not used
    ],
    'mimes' => 'The :attribute must be a file of type: :values.', // not used
    'mimetypes' => 'The :attribute must be a file of type: :values.', // not used
    'min' => [
        'numeric' => 'The :attribute must be at least :min.', // not used
        'file' => 'The :attribute must be at least :min kilobytes.', // not used
        'string' => ':attribute laukas turi būti bent :min raidžių.',
        'array' => 'The :attribute must have at least :min items.', // not used
    ],
    'multiple_of' => 'The :attribute must be a multiple of :value', // not used
    'not_in' => 'The selected :attribute is invalid.', // not used
    'not_regex' => 'The :attribute format is invalid.', // not used
    'numeric' => 'The :attribute must be a number.', // not used
    'password' => 'The password is incorrect.', // not used
    'present' => 'The :attribute field must be present.', // not used
    'regex' => 'The :attribute format is invalid.', // not used
    'required' => ':attribute laukas yra būtinas.',
    'required_if' => 'The :attribute field is required when :other is :value.', // not used
    'required_unless' => 'The :attribute field is required unless :other is in :values.', // not used
    'required_with' => 'The :attribute field is required when :values is present.', // not used
    'required_with_all' => 'The :attribute field is required when :values are present.', // not used
    'required_without' => 'The :attribute field is required when :values is not present.', // not used
    'required_without_all' => 'The :attribute field is required when none of :values are present.', // not used
    'same' => 'The :attribute and :other must match.', // not used
    'size' => [
        'numeric' => 'The :attribute must be :size.', // not used
        'file' => 'The :attribute must be :size kilobytes.', // not used
        'string' => 'The :attribute must be :size characters.', // not used
        'array' => 'The :attribute must contain :size items.', // not used
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.', // not used
    'string' => 'The :attribute must be a string.', // not used
    'timezone' => 'The :attribute must be a valid zone.', // not used
    'unique' => 'The :attribute has already been taken.', // not used
    'uploaded' => 'The :attribute failed to upload.', // not used
    'url' => 'The :attribute format is invalid.', // not used
    'uuid' => 'The :attribute must be a valid UUID.', // not used

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'password' => 'Slaptažodio',
        'title' => 'Pavadinimo',
        'message' => 'Žinutės',
        'video' => 'Video failo'
    ],

];