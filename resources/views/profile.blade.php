@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 my-2">
            <div class="card dark:bg-gray-200">
                @if(Auth::user() == null)
                <div class="card-header">{{ __('profile.profile-name', ['name' => $user->name]) }} {{ __('profile.profile') }}</div>
                @elseif(Auth::user()->id != $user->id)
                <div class="card-header">{{ __('profile.profile-name', ['name' => $user->name]) }} {{ __('profile.profile') }}</div>
                @elseif(Auth::user()->id == $user->id)
                <div class="card-header">{{ __('profile.profile-name', ['name' => $user->name]) }} {{ __('profile.profile') }} <a href="{{ URL::to('/') }}/profile/{{ $user->username }}/edit" class="fas fa-edit"></a></div>
                @endif
                <div class="card-body">
                    {{ __('profile.displayname') }}: {{ $user->name }}
                    <br>
                    {{ __('profile.username') }}: {{ $user->username }}
                    <br>
                    {{ __('profile.role') }}: 
                    @if($user->role == 0)
                        {{ __('profile.user') }}
                    @elseif($user->role == 1)
                        {{ __('profile.uploader') }}
                    @elseif($user->role == 2)
                        {{ __('profile.administrator') }}
                    @endif
                    <br>
                    {{ __('profile.registered-ago') }}: {{ $registered_ago }}
                    <br>
                    {{ ucwords(__('profile.posts')) }}: {{ App\Models\Post::where('author_id', $user->id)->count() }}
                    <br>
                    {{ __('profile.likes-received') }}: {{ $likes_received }}
                    <br>
                    @if(Auth::user() != null && $user->id == Auth::user()->id && Auth::user()->api_token != null)
                    {{ __('profile.api-token') }}: {{ Auth::user()->api_token }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8 my-2">
            <div class="card dark:bg-gray-200">
                <div class="card-header">{{ __('profile.profile-name', ['name' => $user->name]) }} {{ __('profile.posts') }}</div>

                <div class="card-body">
                    @foreach($posts as $post)
                    @if($post->author_id == $user->id)

                    <div class="card my-2 dark:bg-gray-100">
                    @if(Auth::user() != null)
                        @if(Auth::user()->id == $post->author_id)
                        <div class="card-header"><b><a href="{{ URL::to('/') }}/post/view/{{ $post->id }}">{{ $post->title }}</a></b> <a href="{{ URL::to('/') }}/post/edit/{{ $post->id }}" class="fas fa-edit"></a> <a href="{{ URL::to('/') }}/post/remove/{{ $post->id }}" class="fas fa-trash-alt"></a></div>
                        @else
                        <div class="card-header"><b><a href="{{ URL::to('/') }}/post/view/{{ $post->id }}">{{ $post->title }}</a></b></div>
                        @endif
                    @else
                        <div class="card-header"><b><a href="{{ URL::to('/') }}/post/view/{{ $post->id }}">{{ $post->title }}</a></b></div>
                    @endif
                        <div class="card-body">{{ $post->message }}<br></div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col text-center">
                                    <form action="{{ route('like') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="post" value="{{ $post->id }}">
                                        <?php $like_count = App\Models\Like::where('post_id', $post->id)->count() ?>
                                        @if(Auth::user() == null)
                                            @if(App\Models\Like::where('post_id', $post->id)->count() == 0)
                                            <input type="submit" class="btn btn-light" disabled value="👍 {{ App\Models\Like::where('post_id', $post->id)->count() }}">
                                            @elseif(App\Models\Like::where('post_id', $post->id)->count()-1 == 0)
                                            <input type="submit" class="btn btn-light" disabled data-toggle="tooltip" data-placement="top" title="{{ __('post.liked-by') }} {{ App\Models\User::where('id', App\Models\Like::where('post_id', $post->id)->first()->user_id)->first()->name }}" value="👍 {{ App\Models\Like::where('post_id', $post->id)->count() }}">
                                            @else
                                            <input type="submit" class="btn btn-light" disabled data-toggle="tooltip" data-placement="top" title="{{ __('post.liked-by-x-and-y-others', ['x' => App\Models\User::where('id', App\Models\Like::where('post_id', $post->id)->first()->user_id)->first()->name, 'y' => App\Models\Like::where('post_id', $post->id)->count()-1]) }}" value="👍 {{ App\Models\Like::where('post_id', $post->id)->count() }}">
                                            @endif
                                        @elseif(App\Models\Like::where('user_id', Auth::id())->where('post_id', $post->id)->exists())
                                            @if(App\Models\Like::where('post_id', $post->id)->count()-1 == 0)
                                            <input type="submit" class="btn btn-light active" data-toggle="tooltip" data-placement="top" title="{{ __('post.liked-by-you') }}" value="👍 {{ App\Models\Like::where('post_id', $post->id)->count() }}">
                                            @else
                                            <input type="submit" class="btn btn-light active" data-toggle="tooltip" data-placement="top" title="{{ __('post.liked-by-you-and-x-others', ['x' => App\Models\Like::where('post_id', $post->id)->count()-1]) }}" value="👍 {{ App\Models\Like::where('post_id', $post->id)->count() }}">
                                            @endif
                                        @else
                                            @if(App\Models\Like::where('post_id', $post->id)->count() == 0)
                                            <input type="submit" class="btn btn-light" value="👍 {{ App\Models\Like::where('post_id', $post->id)->count() }}">
                                            @elseif(App\Models\Like::where('post_id', $post->id)->count()-1 == 0)
                                            <input type="submit" class="btn btn-light" data-toggle="tooltip" data-placement="top" title="{{ __('post.liked-by') }} {{ App\Models\User::where('id', App\Models\Like::where('post_id', $post->id)->first()->user_id)->first()->name }}" value="👍 {{ App\Models\Like::where('post_id', $post->id)->count() }}">
                                            @else
                                            <input type="submit" class="btn btn-light" data-toggle="tooltip" data-placement="top" title="{{ __('post.liked-by-x-and-y-others', ['x' => App\Models\User::where('id', App\Models\Like::where('post_id', $post->id)->first()->user_id)->first()->name, 'y' => App\Models\Like::where('post_id', $post->id)->count()-1]) }}" value="👍 {{ App\Models\Like::where('post_id', $post->id)->count() }}">
                                            @endif
                                        @endif
                                    </form>
                                </div>
                                <div class="hidden" style="display: none;">
                                    {{ $post_date = $post->created_at }}
                                    {{ $post_date_time = new \Carbon\Carbon($post_date) }}
                                    {{ $post_update = $post->updated_at }}
                                    {{ $post_update_time = new \Carbon\Carbon($post_update) }}
                                </div>
                                <div class="col text-center">
                                    <small class="text-muted">{{ __('post.created') }}: {{ $post_date_time->diffForHumans() }}</small>
                                    @if($post->created_at != $post->updated_at)
                                    <br><small class="text-muted">{{ __('post.updated') }}: {{ $post_update_time->diffForHumans() }}</small>
                                    @endif
                                </div>
                                <div class="col text-center">
                                    <small class="text-muted">{{ __('post.author') }}: <a href="{{ URL::to('/') }}/profile/{{ App\Models\User::where('id', $post->author_id)->firstOrFail()->username }}">{{ App\Models\User::where('id', $post->author_id)->firstOrFail()->name }}</a></small>
                                </div>
                            </div>
                        </div>
                    </div>


                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    }, false);
</script>
@endsection
