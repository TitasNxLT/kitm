@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <!-- Success message -->
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
            @endif
            <div class="card dark:bg-gray-200">
                <div class="card-body">
                    <form method="post" action="{{ route('edit_post') }}" enctype="multipart/form-data">

                        @csrf
                        <input type="hidden" name="id" value="{{ $post->id }}">
                        <div class="form-group">
                            <label>{{ __('post.title') }}</label>
                            <input type="text" class="form-control" name="title" id="title" value="{{ $post->title }}">
                        </div>
                        @if ($errors->has('title'))
                        <div class="error">
                            {{ $errors->first('title') }}
                        </div>
                        @endif

                        <div class="form-group">
                            <label>{{ __('post.message') }}</label>
                            <textarea class="form-control" name="message" id="message" rows="4">{{ $post->message }}</textarea>
                        </div>
                        @if ($errors->has('message'))
                        <div class="error">
                            {{ $errors->first('message') }}
                        </div>
                        @endif

                        <div class="embed-responsive embed-responsive-16by9 card-img-top">
                            <video class="embed-responsive-item" src="{{ asset('/storage/videos/'.$post->id.'.mp4') }}" autoplay controls loop>
                        </div>

                        <div class="form-group">
                            <label>{{ __('post.video-file') }}</label>
                            <input class="form-control" type="file" id="video" name="video" accept=".mp4">
                        </div>
                        @if ($errors->has('video'))
                        <div class="error">
                            {{ $errors->first('video') }}
                        </div>
                        @endif

                        <input type="submit" name="send" value="{{ __('post.edit-post') }}" class="btn btn-dark btn-block">
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection