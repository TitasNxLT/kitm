@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('main.admin_panel') }} - {{ __('admin.users') }}</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">{{ __('admin.displayname') }} ({{ __('admin.username') }})</th>
                                <th scope="col">{{ __('admin.role') }}</th>
                                <th scope="col">{{ __('admin.api-token') }}</th>
                                <th scope="col">{{ __('admin.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <th>{{ $user->id }}</th>
                                <th><a href="{{ URL::to('/') }}/profile/{{ $user->username }}">{{ $user->name }} ({{ $user->username }})</a></th>
                                <th>@if($user->role == 0) {{ __('admin.user') }} @elseif($user->role == 1) {{ __('admin.uploader') }} @elseif($user->role == 2) {{ __('admin.administrator') }} @else {{ __('admin.error') }} @endif</th>
                                <th>@if($user->api_token == null) {{ __('admin.api-token-not-set') }} <a href="{{ URL::to('/') }}/profile/{{ $user->username }}/gen-api">{{ __('admin.generate-api') }}</a> @else {{ $user->api_token }} <a>{{ __('admin.revoke-api') }}</a> @endif</th>
                                <th>
                                    <form action="{{ route('admin_panel.users') }}" method="post" style="display: inline-block;">
                                        @csrf
                                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                                        <input type="hidden" name="executor" value="{{ Auth::user()->id }}">
                                        <input type="hidden" name="action" value="up">
                                        <input type="submit" value="⬆" class="btn btn-info">
                                    </form>
                                    <form action="{{ route('admin_panel.users') }}" method="post" style="display: inline-block;">
                                        @csrf
                                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                                        <input type="hidden" name="executor" value="{{ Auth::user()->id }}">
                                        <input type="hidden" name="action" value="down">
                                        <input type="submit" value="⬇" class="btn btn-info">
                                    </form>
                                </th> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
