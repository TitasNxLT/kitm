@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('main.admin_panel') }}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <div class="card">
                                <div class="card-header"><b>{{ __('admin.statistics') }}</b></div>
                                <div class="card-body">
                                    <p class="card-text"><b>{{ __('admin.posts') }}:</b> {{ App\Models\Post::count() }}</p>
                                    <p class="card-text"><b>{{ __('admin.users') }}:</b> {{ App\Models\User::count() }}</p>
                                    <p class="card-text"><b>{{ __('admin.used-space') }}:</b> {{ $file_size }}MB</p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card">
                                <div class="card-header"><a href="{{ route('admin_panel.users') }}">{{ __('admin.users') }}</a></div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card">
                                <div class="card-header"><a href="{{ route('admin_panel.posts') }}">{{ __('admin.posts') }}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
