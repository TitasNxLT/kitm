@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('main.admin_panel') }} - {{ __('admin.posts') }}</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">{{ __('admin.title') }}</th>
                                <th scope="col">{{ __('admin.author') }}</th>
                                <th scope="col">{{ __('admin.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                            <tr>
                                <th>{{ $post->id }}</th>
                                <th>{{ $post->title }}</th>
                                <th><a href="{{ URL::to('/') }}/profile/{{ $author = App\Models\User::where('id', $post->author_id)->first()->name }}">{{ $author }}</a></th>
                                <th>
                                    <form action="{{ route('admin_panel.posts') }}" method="post" style="display: inline-block;">
                                        @csrf
                                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                                        <input type="hidden" name="executor" value="{{ Auth::user()->id }}">
                                        <input type="hidden" name="action" value="remove">
                                        <button type="submit" class="fas fa-trash-alt btn btn-light"></button>
                                    </form>
                                    <a class="btn btn-light fas fa-eye" href="{{ URL::to('/') }}/post/view/{{ $post->id }}"></a>
                                </th> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
