@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <!-- Success message -->
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
            @endif
            <div class="card dark:bg-gray-200">
                <div class="card-body">
                    <form method="post" action="{{ route('edit_profile') }}" enctype="multipart/form-data">

                        @csrf
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <div class="form-group">
                            <label>{{ __('profile.username') }}</label>
                            <input type="text" class="form-control" name="username" id="username" disabled value="{{ $user->username }}">
                        </div>

                        <div class="form-group">
                            <label>{{ __('profile.displayname') }}</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}">
                        </div>
                        @if ($errors->has('name'))
                        <div class="error">
                            {{ $errors->first('name') }}
                        </div>
                        @endif
                        <div class="form-group">
                            <label>{{ __('profile.email') }}</label>
                            <input type="text" class="form-control" name="email" id="email" disabled value="{{ $user->email }}">
                        </div>

                        <input type="submit" name="send" value="{{ __('profile.edit-profile') }}" class="btn btn-dark btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection